const missingNum = (arr) => {
    const minNum = Math.min(...arr);
    const maxNum = Math.max(...arr);
  
    for (let i = minNum; i <= maxNum; i++) {
      if (!arr.includes(i)) {
        return i;
      }
    }
  
    return null;
  };
  
  console.log(missingNum([1, 2, 3, 9, 4, 5, 6, 8, 10]));
  
  
    
