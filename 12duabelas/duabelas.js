const spiralOrder = (matrix) => {
    const centerMatrix = matrix[1];
    const s1 = matrix[0];
    const s2 = centerMatrix.slice(-1);
    const s3 = matrix[2].reverse();
    const s4 = centerMatrix.slice(0, -1);
    return [s1, s2, s3, s4].flat();
  };
  
  console.log(
    spiralOrder([
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
    ])
  );

  console.log(
    spiralOrder([
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9,10,11,12]
    ])
  );

  console.log(
    spiralOrder([
        [1, 2, 3, 4, 5],
        [6, 7, 8, 9, 10],
        [11, 12, 13, 14, 15]
    ])
  );
  