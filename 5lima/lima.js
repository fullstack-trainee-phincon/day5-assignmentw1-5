const getBudget = (arr,property) => {
    const total = arr.reduce((a,b) => {
        return a + b[property]
    },0);

    return total
}

const arr = [
    { name: "John", age: 21, budget: 23000 },
    { name: "Steve",  age: 32, budget: 40000 },
    { name: "Martin",  age: 16, budget: 2700 }
  ]
console.log("budget", getBudget(arr,"budget"))