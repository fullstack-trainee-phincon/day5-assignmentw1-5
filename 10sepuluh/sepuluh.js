const numberPairs = (str) => {
  let count = 0;
  const data = str.slice(1).split(" ").sort();
  for (let i = 0; i < data.length; i++) {
    if (data[i] === null) continue;
    if (data[i] === data[i + 1]) {
      count++;
      data[i + 1] = null;
    }
  }
  return count;
};

console.log(numberPairs("9 10 20 20 10 10 30 50 10 20"));
