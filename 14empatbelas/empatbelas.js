const whoWon = (board) => {
    const r1 = board[0];
    const r2 = board[1];
    const r3 = board[2];
  
    if (r1[0] === r1[1] && r1[0] === r1[2]) return r1[0];
    if (r2[0] === r2[1] && r2[0] === r2[2]) return r2[0];
    if (r3[0] === r3[1] && r3[0] === r3[2]) return r3[0];
    if (r1[0] === r2[0] && r1[0] === r3[0]) return r1[0];
    if (r1[1] === r2[1] && r1[1] === r3[1]) return r1[1];
    if (r1[2] === r2[2] && r1[2] === r3[2]) return r1[2];
    if (r1[0] === r2[1] && r1[0] === r3[2]) return r1[0];
    if (r1[2] === r2[1] && r1[2] === r3[0]) return r1[2];
    return "TIE";
  };
  
  console.log(
    whoWon([
      ["O", "X", "O"],
      ["X", "X", "O"],
      ["O", "X", "X"],
    ])
  );

  console.log(whoWon([
    ["O", "O", "X"],
    ["X", "O", "X"],
    ["O", "X", "O"]
  ])
  )
  