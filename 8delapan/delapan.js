const inBox = (arr) => {
    const boxString = arr.join('\n');
    const regex = /#\*#[^#\n]*#\*#/;
  
    return regex.test(boxString);
  };
  
  console.log(inBox([
    "####",
    "#* #",
    "#  #",
    "####"
  ]));
  