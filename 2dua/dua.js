const makeTitle = (sentence) => {
    const words = sentence.split(" ")
    const capitalizedWords = words.map((word) => word.charAt(0).toUpperCase() + word.slice(1)) 
    const capitalizedSentence = capitalizedWords.join(" ")
    return capitalizedSentence
}

console.log(makeTitle("This is a title"))
console.log(makeTitle("capitalize every word"))
console.log(makeTitle("I am a title"))
