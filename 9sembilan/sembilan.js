const awardPrizes = (data) => {
    const sortedScore = Object.entries(data).sort((a,b) => b[1] - a[1]);

    const result = {}
    for (let i=0;i<sortedScore.length; i++) {
        const [name, score] = sortedScore[i];
        if (i === 0) {
            result[name] = "gold";
        } else if (i === 1) {
            result[name] ='silver';
        } else if (i === 2 ) {
            result[name] = 'bronze'
        }
    } ;
    return result;
}

console.log(awardPrizes({
    "Joshua" : 45,
    "Alex" : 39,
    "Eric" : 43
  }
  ))