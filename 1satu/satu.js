const smile = ":D"
const grin = ":)"
const sad = ":("
const mad = ":p"

const emotify = (str) => {
    const smile = ":D"
    const grin = ":)"
    const sad = ":("
    const mad = ":p"

    const replaceStr = str
    .replace(/smile/gi, smile)
    .replace(/grin/gi, grin)
    .replace(/sad/gi, sad)
    .replace(/mad/gi, mad);
    return replaceStr
}

const originalStr = "Make me Smile, Make me grin, Make me Sad, Make me Mad"
const modifiedStr = emotify(originalStr)

console.log(modifiedStr)