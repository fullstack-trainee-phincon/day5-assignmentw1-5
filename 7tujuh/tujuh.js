const reverseWord = (word) => {
    let reverseWord = word.split(" ").reverse().join(" ")
    return reverseWord
}
console.log(reverseWord("a good example")); 
console.log(reverseWord("the sky is blue")); 
